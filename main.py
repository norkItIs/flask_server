"""
Single entry point that resolves the import dependencies

When running the app, you point to main.py or 'main.app'
"""
from app import app, db

from models import *
from views import *


if __name__ == '__main__':
    app.run()
