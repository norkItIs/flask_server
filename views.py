"""
views imports app, and models, but none of these import views
"""
from flask import json, render_template

from app import app
""" DB Example
from app import db
from models import Task
"""


@app.route("/", methods=['GET'])
def index():
    return render_template('index.html')
