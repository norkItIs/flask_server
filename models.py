"""
models imports app, but app does not import models so we haven't created any
loops.
"""

""" Example using peewee 
from peewee import *

from app import db

class Task(db.Model):
    id = CharField()
    name = CharField()
    start = CharField()
    end = CharField()
    progress = IntegerField()
    dependencies = CharField()
    custom_class = CharField()

    def __unicode__(self):
        return self.id
"""