#!/usr/bin/env python3

from flask import Flask  #, json, render_template



app = Flask(__name__)
app.config.from_object('config.Configuration')

""" DB example
from flask_peewee.db import Database
db = Database(app)
"""
